#!/usr/bin/env node

var cron = require('node-cron');
let tool = require('./tools')
var fs = require('fs');

var config = JSON.parse(fs.readFileSync('config.json', 'utf8'));
console.log("Starting bot with config:")
console.log(JSON.stringify(config, null, 4))

//tool.register(config['token'], "Tester", config["groups"]["me"], config["callback"], config["avatar"])
//tool.post("I'm Back!", config["bots"]["tester"])

// Water Reminder #1
cron.schedule('0 0 11 * * *', () => {
  msg = 'Remember to stay hydrated.'
  tool.post(msg, config["bots"]["whitesides"])
  console.log('Water Reminder #1')
});
// Water Reminder #2
cron.schedule('0 0 14 * * *', () => {
  msg = 'Get drinking!'
  tool.post(msg, config["bots"]["whitesides"])
  console.log('Water Reminder #2')
});
// Water Reminder #3
cron.schedule('0 0 17 * * *', () => {
  msg = 'Have you drunk enough today?'
  tool.post(msg, config["bots"]["whitesides"])
  console.log('Water Reminder #3')
});

// Mobility Inspiration
cron.schedule('0 0 12,20 * * *', () => {
  rand = Math.floor(Math.random() * config["stretches"].length)
  msg = ['Need some mobility inspiration?', ('How about ' + config["stretches"][rand] + '?')]
  tool.multi(msg, config["bots"]["whitesides"])
  console.log('Mobility Inspiration')
});

// 10pm Reminder
cron.schedule('0 0 22 * * *', () => {
  msg = 'Have you drank enough water and done mobility today?'
  tool.post(msg, config["bots"]["whitesides"])
  console.log('10pm Reminder')
});

// Weekday 6:30am Reminder
cron.schedule('0 30 6 * * 0-5', () => {
  msg = ['Today you need to:', 'Drink plenty of water', 'Complete mobility']
  tool.multi(msg, config["bots"]["whitesides"])
  console.log('Weekday 6:30 Reminder')
});
// Saturday 6:30am Reminder
cron.schedule('0 30 6 * * 6', () => {
  msg = ['Today you need to:', 'Do extra fitness', 'Drink plenty of water', 'Complete mobility']
  tool.multi(msg, config["bots"]["whitesides"])
  console.log('Saturday 6:30 Reminder')
});

// Organize Workout
cron.schedule('0 30 16 * * 5', () => {
  msg = 'Should we workout together tomorrow?'
  tool.post(msg, config["bots"]["whitesides"])
  console.log('Organize Workout')
});


//tester
if (false) {
  cron.schedule('*/10 * * * * *', () => {
    rand = Math.floor(Math.random() * config["stretches"].length)
    msg = ['Need some mobility inspiration?', 'How about ' + config["stretches"][rand] + '?']
    tool.multi(msg, config["bots"]["tester"])
    console.log('Mobility Inspiration')
  });
}
