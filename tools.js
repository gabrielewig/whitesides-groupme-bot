module.exports = {
  callback: function(error, response, body) {
    if (!error) {
      if (body != " ") console.log(body)
    } else {
      console.log("ERROR!")
      console.log(error)
      console.log("RESPONSE:")
      console.log(response)
    }
  },

  ids: function (token) {
    var request = require('request')
    var data = {
      url: 'https://api.groupme.com/v3/groups?token=' + token
    }
    request(data, module.exports.callback)
  },

  register: function (token, name, group_id, callback_url, avatar_url) {
    var data = {
      url: 'https://api.groupme.com/v3/bots?token=' + token,
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: ('{"bot": { "name": "' + name
             + '", "group_id": "' + group_id
             + '", "callback_url": "' + callback_url
             + '", "avatar_url": "' + avatar_url + '"}}')
    }
    var request = require('request')
    request(data, module.exports.callback)
  },

  post: function (message, bot_id) {
    var body = '{"bot_id": "' + bot_id + '", "text": "' + message +'"}'
    packet = {
      url: 'https://api.groupme.com/v3/bots/post',
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: body
    }
    var request = require('request')
    request(packet, module.exports.callback)
  },

  multi: function (list, bot_id) {
    for(var i in list){
      var msg = list[i]
      setTimeout(module.exports.post, 2000*i, msg, bot_id);
    }
  },

  sleep: function (milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
      if ((new Date().getTime() - start) > milliseconds){
        break;
      }
    }
  }
}
